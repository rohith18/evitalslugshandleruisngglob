import json
import glob

def main():
    files = glob.glob("./evital_slugs/evital_slugs*.json")

    dict_file = {}
    count = 0

    for file in files:
        with open(file) as file:
            dict_file = { **dict_file, **(json.load(file)) }

    for key in dict_file:
        if dict_file[key] != None and dict_file[key]["data"] != None and dict_file[key]["data"]["medicine_name"] != None:
            print(dict_file[key]["data"]["medicine_name"])
            count = count + 1

    print("Total Entries: ", count)

if __name__ == "__main__":
    main()



